import '../../../styles/index.scss';
import dateFormat from "dateformat";


class Checkout{
    #orders = []
    #checkoutTime
    #restaurant
    constructor(orders, checkout, restaurant) {
        this.#checkoutTime = checkout;
       
        if (!Array.isArray(orders)) {
            throw new Error('Not an Array!'); 
        }

        orders.forEach(el => {
            if (el.id < 0 || el.price < 0 || el.title.length > 30 || el.title.length < 5 || el.count < 0  || Object.keys(el).length !== 4)  {
                throw new Error('Invalid data!');
            }
        });
        this.#orders = orders;

        const allRestaurants = ['Domino’s Pizza', 'McDonald’s',
         'KFC'] ;
        if (!allRestaurants.includes(restaurant)) {
            throw new Error('Missing restaurant title!');
        } else {
            this.#restaurant = restaurant;
        }          
    }


    get isOrderFinished() {
        
    return ((new Date().getTime() - new Date(this.#checkoutTime).getTime()) / 1000 / 60) >= 60; 
    }

    getRestaurant() {
        return this.#restaurant;
    }

    getCheckoutTime() {
        return  Math.round(60 - (new Date().getTime() - new Date(this.#checkoutTime).getTime()) /1000/60); 
    }

    getFormattedDate() {
        return dateFormat( new Date(this.#checkoutTime), "mmmm dS, yyyy"); // new Date(this.#checkoutTime)
    }

    getFormattedTime() {
        return dateFormat( new Date(this.#checkoutTime), " h:MM TT");
    }

    getOrders() {
        return this.#orders;
    }

    getCheckoutTimePercent() {
return  Math.round(this.getCheckoutTime() / 60 * 100);
    }
}

const ordersBtn = document.getElementsByClassName(' navigation__item--orders')[0];
const productsBtn = document.getElementsByClassName(' navigation__item--choice')[0];
ordersBtn.classList.add( 'selected');
productsBtn.classList.remove( 'selected');




const allOrdersFromLocal = JSON.parse(localStorage.getItem('orders')); // array of objects non private
const totalOrdersInstances = allOrdersFromLocal.map(el=>  new Checkout(el.orders, el.checkout, el.restaurant)); //private


const previousOrders = [];
const activeOrders = [];


totalOrdersInstances.map(el => {
    if (el.isOrderFinished) {
        previousOrders.push(el);
    } else {
        activeOrders.push(el);
    }
});
console.log(activeOrders);

function drawPrevOrder(prevOrderObj) {

    return `
    <div class="previous__item previous-item">
          <div class="previous-item__header">
            <h4 class="h4">${prevOrderObj.getRestaurant()}</h4>
            <div class="badge badge--green">Completed</div>
          </div>

          <div class="previous-item-info">
            <div class="previous-item-info__date">${prevOrderObj.getFormattedDate() }</div>
            <div class="previous-item-info__time">${ prevOrderObj.getFormattedTime()}</div>
          </div>

          <ul  class="previous-item-dishes">
          ${prevOrderObj.getOrders().map(el => 
              `<li class="previous-item-dishes__item">
            <span class="previous-item-dishes__quantity">${el.count}</span>
            ${el.title}
          </li>`).join('')}
                      
          </ul>
        </div>
    `;
}

function generatePrevOrderHTML(previousOrders) {
    const previous = document.getElementsByClassName('previous')[0];
    previous.innerHTML = '';
    const previousOrdersList = previousOrders.map(item => drawPrevOrder(item)).join('');
    previous.innerHTML = previousOrdersList;
}

generatePrevOrderHTML(previousOrders);


function drawActiveOrder(activeOrderObj) {
    return `
    <div class="coming-up__item coming-up-item">
        <div class="coming-up-item__header">
          <h4 class="h4">${activeOrderObj.getRestaurant()}</h4>
          <div class="badge badge--orange">Delivery</div>
        </div>

        <div class="coming-up-info">
            <img src="img/icons/clock.svg" alt="">
            <div class="coming-up-info__content">
              <div>Order will be completed in</div>
              <div class="coming-up-info__title">${activeOrderObj.getCheckoutTime()} min</div>
            </div>
          </div>

          <div class="progress-bar">
            <div class="progress-bar__line" style="width: ${activeOrderObj.getCheckoutTimePercent()}%" ></div>
            <div class="progress-bar__overlay">
              <div class="progress-bar__item progress-bar__item--first"> </div>
              <div class="progress-bar__item progress-bar__item--sec"></div>
              <div class="progress-bar__item progress-bar__item--third"></div>
            </div>
          </div>
        </div> `;
}


function generateActiveOrderHTML(activeOrders) {
    const active = document.getElementsByClassName('coming-up')[0];
    active.innerHTML = '';
    const activeOrdersList = activeOrders.map(item => drawActiveOrder(item)).join('');
    active.innerHTML = activeOrdersList;
}

generateActiveOrderHTML(activeOrders);


