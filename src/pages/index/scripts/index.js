import '../../../styles/index.scss';
import dominosArray from './data/dominos.json';
import kfcArray from './data/kfc.json';
import macArray from './data/mac.json';

//TASK 1

class Dish{
    #count = null
    constructor(id, price, title, img, count) {
        this.id = id;
        this.price = price;
        this.title = title;
        this.img = img;
        this.#count = count;
    }
    getCount() {
        return this.#count;
    }
    setCount(count) {
        if (typeof count === 'number' && !isNaN(count) && count >= 0) {
           this.#count = count;
        } else {
            throw new Error('Invalid data!');
        }
    } 
}

//convert JSONs to array w/dish objects

const dominoDishes = dominosArray.map((item) => new Dish(item.id, item.price, item.title, item.img, item.count));
const kfcDishes = kfcArray.map((item) => new Dish(item.id, item.price, item.title, item.img, item.count));
const macDishes = macArray.map((item) => new Dish(item.id, item.price, item.title, item.img, item.count));


const allRestaurants = document.querySelectorAll('.featured__item'); //node List  - статическая коллекция
const cart = document.getElementById('cartBtn'); 
const cartQuantityIcon = document.getElementsByClassName('icon-button__badge')[0];
const drawer = document.getElementById('drawer');
const close = document.querySelectorAll('.icon-button')[1];
const checkoutBtn = document.getElementById('checkout');
const dishesDatabase = [...dominoDishes, ...kfcDishes, ...macDishes];
    
const defaultMenu = defineSelectedMenu();
generateHtML(defaultMenu);


// отображаем выбранное меню на странице
 allRestaurants.forEach(item => {   
    item.addEventListener('click', () => {
        allRestaurants.forEach(item => item.classList.remove('active'));
        item.classList.add('active');
        // dishesList.innerHTML = '';  
        const chosenMenu = defineSelectedMenu();
        generateHtML(chosenMenu);
        displayMinusBtn();
        cartQuantityTotal();

    });
 });


 //связываем выбранный ресторан и массивы с меню посредством id
function defineSelectedMenu() {
    const activeRest = document.getElementsByClassName('active featured__item featured-item ')[0];
    if (activeRest.id === 'domino') {
        return dominoDishes;
    } else if (activeRest.id === 'mac') {
        return macDishes;
    } else if (activeRest.id === 'kfc') {
        return kfcDishes;
    }      
}
 

//generate html for 1 card
function display1Dish(dish) {    
    return ` 
    <div id="dish-${dish.id}" class="dish">
        <img class="dish__image" src="${dish.img}" alt="">
        <div class="dish__title">${dish.title}</div>
        <div class="dish__info">
            <div class="dish__price">${dish.price} $</div>
            <div  class="counter">
                <button  class="counter__button counter__button--decrease"></button>
                <span class="counter__number">${dish.getCount()}</span>
                <button class="counter__button counter__button--increase"></button>
            </div>
        </div>
    </div>`;
}

//create whole menu list - 6 items создаем разметку для отображения полного меню
function generateHtML(menuData) {
    const dishesList = document.getElementById('dishes'); //убираем карточки, чтобы наполнить новыми при клике
    dishesList.innerHTML = '';                              //
   const menuList =  menuData.map(item => display1Dish(item)).join('');  
    dishesList.innerHTML = menuList;  
    increaseBtnFunction();
    decreaseBtnFunction();
}

function displayMinusBtn() {
    const counterValue = Array.from(document.getElementsByClassName('counter__number'));// HTML collection, 'live', doesn't have forEach method
    counterValue.map(item => {
        const minus = item.previousElementSibling;
            if (item.textContent === '0') {
               minus.setAttribute('style', 'display: none');                
            } else {               
               minus.setAttribute('style', 'display: inherit');
        }
    });      
}
displayMinusBtn();

function increaseBtnFunction() {
    const increaseBtn = Array.from(document.getElementsByClassName('counter__button--increase')); 
    increaseBtn.forEach(item => {
        item.addEventListener('click', () => {     
           +item.previousElementSibling.innerText++; 
            displayMinusBtn(); 
            cartQuantityTotal();          
        });
    });
}

function decreaseBtnFunction() {
    const decreaseBtn = Array.from(document.getElementsByClassName(' counter__button--decrease'));
    decreaseBtn.forEach(item => {
        item.addEventListener('click', () => {
            +item.nextElementSibling.innerText--; 
            displayMinusBtn();  
            cartQuantityTotal();
        });
    });
}

const mac = document.getElementById('mac');
mac.setAttribute('name', 'McDonald’s');

const kfc = document.getElementById('kfc');
kfc.setAttribute('name', 'KFC');

const dominos = document.getElementById('domino');
dominos.setAttribute('name', 'Domino’s Pizza');


cart.addEventListener('click', () => {
    drawer.classList.add('visible');
    const orderSubtitle = document.getElementsByClassName('subtitle')[0];
    orderSubtitle.innerText = cartQuantityIcon.innerText + ' item';
    const activeRest = document.getElementsByClassName('active featured__item featured-item ')[0];
    const restTitle = document.getElementById('restTitle');
    restTitle.innerText = activeRest.name; 
    const data = formOrderStructure();
    generateCartHtml(data);
});


//does same as add items btn???
close.addEventListener('click', () => {
    drawer.classList.remove('visible');
});

const addMore = document.getElementsByClassName('add-more')[0];
addMore.addEventListener('click', function () {
    drawer.classList.remove('visible');
});

function cartQuantityTotal() {
    const counterValue = Array.from(document.getElementsByClassName('counter__number'));
    let count = 0;
    counterValue.map(item => {
        if (+item.textContent > 0) {
            count++;           
        }    
    });
     cartQuantityIcon.innerText  = count;
}

// 3. создаем массив данных - массив с объектом
function formOrderStructure() {
    const orderStr = [];
    let totalSum = 0;
    const total = document.getElementById('total');
   const counterValue = Array.from(document.getElementsByClassName('counter__number'));
    console.log(counterValue);
    const activeRest = document.getElementsByClassName('active featured__item featured-item ')[0];
    counterValue.map(item => {
        if (+item.innerText > 0) {
            const title = item.closest('.dish__info').previousElementSibling.innerText;
            const id = dishesDatabase.filter(el => el.title === title)[0].id; 
            const price = item.closest('.counter').previousElementSibling.innerText.slice(0, -1);
            // console.log(price);
            orderStr.push({ id, title, price, count: +item.innerText }); //order
            // console.log( typeof price);//ok string
            // console.log(title);//ok
            // console.log( typeof item.innerText);//ok - count string
            totalSum += Number(price) * Number(item.innerText);  
            //    console.log(totalSum);
        }        
    });
   
    const cartDeliveryPrice = document.getElementById('deliveryPrice');
      
    cartDeliveryPrice.textContent = activeRest.name === 'McDonald’s' ? '20 $' : 'Free delivery';   
    totalSum = activeRest.name === 'McDonald’s' ? totalSum + 20 : totalSum;  

    total.innerText = ` (${totalSum} $)`;
 
    return orderStr;
  };


//1. Сначала отрисовываем 1 строку ордера(одно наименование) с html, в dish придет generateHtml(), одно блюдо
function drawerOrderItem(dish) {
    const dishImage = dishesDatabase.filter(item => item.title === dish.title)[0]; /// 

return ` 
         <div  class="order__item order-item">  
        <img class="order-item__image" src="${dishImage.img}" alt="">
        <span class="order-item__quantity">${dish.count} X</span>
  <div class="order-item__info">
        <h3 class="order-item__title h3">${dish.title}</h3>
        <div class="order-item__price">${dish.price}</div>
  </div>
  <button class="icon-button icon-button--red"><img src="img/icons/delete.svg" alt=""></button>
  </div>`;
};


//2. Затем генерируем разметку, в эту ф-цию приходит структура(массив) с массивом данных (ф-ция formOrderStructure , переменная orderStr)
function generateCartHtml(orderData) { // объект из массива orderStr
    // console.log(orderData);
    let ordersCartList = document.getElementsByClassName('order')[0];
    ordersCartList.innerHTML = '';// optional
    const ordersList = orderData.map(item => drawerOrderItem(item)).join(''); // eto dish
    ordersCartList.innerHTML = ordersList;
    deleteOrderLines();
 }

function deleteOrderLines() {
    const removeCartItemButtons = document.getElementsByClassName('icon-button icon-button--red');
    for (let i = 0; i < removeCartItemButtons.length; i++) {
        const button = removeCartItemButtons[i];
        button.addEventListener('click', function (event) {
            // const buttonClicked = event.target;
            // buttonClicked.parentElement.parentElement(remove); // Kyle's video
            button.parentElement.remove();
                   });
    }
}

deleteOrderLines();


    checkoutBtn.addEventListener('click', () => {
        const allOrders = JSON.parse(localStorage.getItem('orders')) || [];
        const activeRest = document.getElementsByClassName('active featured__item featured-item ')[0];
        const orderObj = {
            restaurant: activeRest.name,
            checkout: new Date(),
            orders: formOrderStructure()
        };   
        allOrders.push(orderObj);
        console.log();
        localStorage.setItem('orders', JSON.stringify(allOrders));
        window.location.href = 'orders.html';
    });
   










